<?php session_start();?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css">
		<?php include "dbConn.php"; ?>
		<?php include "functions.php"; ?>
		<?php include "dbFunctions.php"; ?>
	</head>
	<body>
		<div class="container-fluid" style="width:65%">
			<?php 
			include "parts/header.php";
			include "parts/menu.php"; 
			if(isset($_GET['login'])){
				include "login.php";
			}
			if(isset($_GET['create'])){
				if(isset($_SESSION['username'])){
					include "add.php";
				}else{
					include "login.php";
				}
			}
			if(!isset($_GET['login']) && !isset($_GET['create'])){
				?><div class="row"><?php
				if(isset($_GET['action'])){
					include "parts/sidebar.php";
					?><div class="col-sm-9 text-center"><?php
						include "parts/content.php";?>
					</div><?php
				}else{
					if(isset($_GET['key'])){?>
                        </div>
                        <div class="row justify-content-center"><?php
                            include "parts/advertisementDescription.php";?>
                        </div><?php
                    }else {
                        ?>
                        <div class="row">
                            <div class="col-sm-12 text-center"><?php
                            include "parts/content.php"; ?>
                            </div><?php
                    }
				}
				?>
				</div><?php
			}
			?>
		</div>
		<?php include "parts/footer.php";  ?>
	</body>
</html>