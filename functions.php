<?php
function menu(){
	$menuItems = dbSelect('menu');
	?>
		<div class="row" style="background-color: #f2f2f2">
			<div class="col text-center">
				<?php foreach($menuItems as $menuItem){?>
					<a href="<?php echo $menuItem['link']; ?>?action=<?php echo $menuItem['category']; ?>"><img src="images/<?php echo $menuItem['image']; ?>" width="5%"><?php echo $menuItem['title']; ?></a>
				<?php }?>
			</div>
		</div><?php
}

function advertisement($adItem, $key){?>
    <div class="col-sm-3" style="background-color:white;">
        <div class="card-group">
            <div class="card">
                <a href="?key=<?php echo $key; ?>"><img src="images/<?php echo $adItem['image'];?>" class="card-img-top" height="20%"></a>
                <div class="card-body">
                    <h5 class="card-title"><?php echo $adItem['name'];?></h5>
                    <p class="card-text"><?php echo $adItem['price'];?>€</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted"><?php echo $adItem['county'];?></small>
                </div>
            </div>
        </div>
    </div>
    <?php
}

function advertisementDescript($adItem, $key){?>
    <div class="col-md-12 card">
        <div class="row no-gutters">
            <div class="col-md-4">
                <a href="?key=<?php echo $key; ?>"><img src="images/<?php echo $adItem['image'];?>" class="card-img" height="20%"></a>
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h7 class="card-title"><?php echo $adItem['name'];?></h7><br />
                    <p class="card-text"><b><?php echo $adItem['price'];?>€</b></p><br />
                    <p class="card-text"><small class="text-muted"><?php echo $adItem['seller_name'];?></small></p>
                </div>
            </div>
        </div>
    </div><?php
}

function locations(){
	$locationItems = dbSelect('locations');
	?>
	<form method="post">
        <select name="searchCounty">
            <option value="toata romania" selected disabled>Toata Romania</option><?php
		    foreach($locationItems as $locationItem){
                if(isset($_POST['searchCounty'])){?>
                    <option value="<?php echo $locationItem['value']?>" <?php if($_POST['searchCounty'] == $locationItem['value']){echo "selected";} ?>><?php echo $locationItem['name']?></option><?php
		        }else{?>
                    <option value="<?php echo $locationItem['value']?>"><?php echo $locationItem['name']?></option><?php
                }
		    }?>
	    </select>
    </form>&nbsp;<?php
}

?>