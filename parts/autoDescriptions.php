<div class="col-sm-6">
    <img src="images/<?php echo $advertisementItem['image']; ?>" width="100%">
    <h4><?php echo ucfirst($advertisementItem['name']); ?></h4>
    <p><?php echo $advertisementItem['description']; ?></p>
    <img src="images/<?php echo $advertisementItem['image1']; ?>" width="100%">
    <img src="images/<?php echo $advertisementItem['image2']; ?>" width="100%">
    <img src="images/<?php echo $advertisementItem['image3']; ?>" width="100%">
</div>
<div class="col-sm-5 text-center">
    <div class="card">
        <div class="card-header">
            <?php echo $advertisementItem['price']; ?> €
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Vandut de: <b><?php echo $advertisementItem['seller_name']; ?></b></li>
            <li class="list-group-item"><a href="">Mesaj</a></li>
        </ul>
    </div>
    <hr>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <p>An: <a href=""><?php echo $advertisementItem['manufacture_year']; ?><a/></p>
            </div>
            <div class="col-sm-6">
                <p>Marca: <a href=""><?php echo $advertisementItem['brand']; ?><a/></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p>Model: <a href=""><?php echo $advertisementItem['model']; ?><a/></p>
            </div>
            <div class="col-sm-6">
                <p>Km: <a href=""><?php echo $advertisementItem['km']; ?><a/></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p>Carburant: <a href=""><?php echo $advertisementItem['fuel']; ?><a/></p>
            </div>
            <div class="col-sm-6">
                <p>Transmisia: <a href=""><?php echo $advertisementItem['gearbox']; ?><a/></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p>Motor: <a href=""><?php echo $advertisementItem['engine']; ?><a/></p>
            </div>
        </div>
    </div>
    <hr>
</div>