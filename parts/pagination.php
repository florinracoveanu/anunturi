<div class="row justify-content-md-center">
	<div class="col-sm-13">
		<ul class="pagination"><?php
			for($i = 1; $i <= $nrOfPages; $i++){
				if($i == $pageNumber){
					if(isset($_GET['action'])){?>
						<li class="page-item active"><a class="page-link" href="?action=<?php echo $_GET['action']; ?>&page=<?php echo $i; ?>"><?php echo $i; ?></a></li><?php
					}else{?>
						<li class="page-item active"><a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li><?php
					}
				}else{
					if(isset($_GET['action'])){?>
						<li class="page-item"><a class="page-link" href="?action=<?php echo $_GET['action']; ?>&page=<?php echo $i; ?>"><?php echo $i; ?></a></li><?php
					}else{?>
						<li class="page-item"><a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li><?php
					}
				}
			}?>
		</ul>
	</div>
</div>