<!--Content-->
<?php
$nrAdsOnPage = 12;
$nrAdsOnRow = 4;
$totalAds = 0;
$action = false;

$adItems = dbSelect("advertisements");
if(isset($_GET['action'])){
    $action = true;
    $nrAdsOnRow = 1;
    $adItems = dbSelect("advertisements",['category' => $_GET['action']]);
    if(isset($_POST['auto_category']) &&($_POST['auto_category'] != 'all')){
        $adItems = dbSelect("advertisements",['category' => $_GET['action'], 'sub_category' => $_POST['auto_category']], 'AND');
        if(isset($_POST['auto_brand']) && ($_POST['auto_brand'] != 'all')) {
            $adItems = dbSelect("advertisements", ['category' => $_GET['action'], 'sub_category' => $_POST['auto_category'], 'brand' => $_POST['auto_brand']], 'AND');
            if(isset($_POST['auto_model']) && ($_POST['auto_model'] != 'all')){
                $adItems = dbSelect("advertisements", ['category' => $_GET['action'], 'sub_category' => $_POST['auto_category'], 'brand' => $_POST['auto_brand'], 'model' => $_POST['auto_model']], 'AND');
            }
        }
    }
    if(isset($_POST['houses_category']) && ($_POST['houses_category'] != 'all')){
        $adItems = dbSelect("advertisements" ,['sub_category' => $_POST['houses_category']]);
    }
    if(isset($_POST['it_category']) && ($_POST['it_category'] != 'all')){
        $adItems = dbSelect("advertisements" ,['sub_category' => $_POST['it_category']]);
    }
    if(isset($_POST['animals_category']) && ($_POST['animals_category'] != 'all')){
        $adItems = dbSelect("advertisements" ,['sub_category' => $_POST['animals_category']]);
    }
}

if(!empty($_POST['search'])){
    $adItems = dbSelect("advertisements" ,['name' => $_POST['search'], 'brand' => $_POST['search'], 'model' => $_POST['search']], 'OR', 'like');
}
if(!empty($_POST['searchCounty'])){
    $adItems = dbSelect("advertisements" ,['county' => $_POST['searchCounty']]);
}


$totalAds = count($adItems);
$nrOfPages = ceil($totalAds/$nrAdsOnPage);
if(isset($_GET['page'])){
	$pageNumber = $_GET['page'];
}else{
	$pageNumber = 1;
}
$start = ($pageNumber - 1)*$nrAdsOnPage;
if(($start + $nrAdsOnPage) < $totalAds){
	$limit = $start + $nrAdsOnPage;
}else{
	$limit = $totalAds;
}
?>

	<div class="container-fluid"><br />
		<div class="row">
			<?php
			for($i = $start; $i < $limit; $i++){
				if(($i%$nrAdsOnRow == 0) && ($i != 0)){?>
					</div><br /><div class="row"><?php
				}
				if($action == false) {
                    advertisement($adItems[$i], $adItems[$i]['id']);
                }else{
                    advertisementDescript($adItems[$i], $adItems[$i]['id']);
                }
			}
			?><hr>
		</div>
	</div><br />
	<?php include "parts/pagination.php"; ?>
	</div>
</div>
