<?php 
if(!isset($_SESSION['username'])){?>
	<br />
	<form method="post">
		<div class="row justify-content-md-center">
			<div class="col-sm-4 text-center">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item" style="width:50%">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Autentificare</a>
					</li>
					<li class="nav-item" style="width:50%">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Cont nou</a>
					</li>
				</ul><br />
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel">
						<input type="text" name="username" class="form-control" placeholder="Nume de utilizator"><br />
						<input type="password" name="password" class="form-control" placeholder="Parola"><br />
						<button type="submit" name="login" class="btn btn-primary">Intra in cont</button>
					</div>
					<div class="tab-pane fade" id="profile" role="tabpanel">
						<input type="text" name="usernamenew" class="form-control" placeholder="Nume de utilizator"><br />
						<input type="password" name="passwordnew" class="form-control" placeholder="Parola"><br />
						<button type="submit" name="newAccount" class="btn btn-primary">Creaza cont</button>
					</div>
				</div>
			</div>
		</div><br />
	</form><?php
}else{?>
	<form method="post">
		<h4>Esti logat ca <?php echo ucfirst($_SESSION['username']);?></h4>
		<a href="">link</a><br />
		<a href="">link</a><br />
		<a href="">link</a><br />
		<a href="">link</a><br />
		<button type="submit" name="logout" href="index.php?logout=">Iesi</button><br />
	</form>
	<?php
}
if(isset($_POST['logout'])){
	session_destroy();
	//header('Location: index.php');
}

if(isset($_POST['login'])){
	$existUser = dbSelect('users', ['username' => $_POST['username'], 'password' => $_POST['password']], 'AND');
	if($existUser != null){
		$_SESSION['username'] = $existUser[0]['username'];
		//header('Location: index.php');
		
	}else{
		echo "date incorecte";
	}
 }
 if(isset($_POST['newAccount'])){
	$availableUsername = dbSelect("users", ['username' => $_POST['usernamenew']]);
	if($availableUsername == null){
		dbInsert("users",['username' => $_POST['usernamenew'], 'password' => $_POST['passwordnew']]);
		$userNew = dbSelect("users", ['username' => $_POST['usernamenew']]);
		$_SESSION['username'] =$userNew[0]['username'];
		//header('Location: index.php');
	}else{
		echo "acest user exista!";
	}
 }
 ?>