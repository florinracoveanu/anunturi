<?php

function dbDelete($table, $id){
    global $mysqlConnect;
    $result = mysqli_query($mysqlConnect, "DELETE FROM $table WHERE id=".intval($id));

    return mysqli_affected_rows($mysqlConnect)>0;
}


function dbUpdate($table, $id, $data){
    global $dbConnection;
    $sets = [];
    foreach ($data as $column => $value){
        $sets[]="`".mysqli_real_escape_string($mysqlConnect,$column)."`='".mysqli_real_escape_string($mysqlConnect,$value)."'";
    }
    $sqlSets = implode(',', $sets);

    $result = mysqli_query($mysqlConnect, "UPDATE $table SET $sqlSets  WHERE id=".intval($id));

    return mysqli_affected_rows($mysqlConnect)>0;

}

function dbInsert($table, $data){
    global $mysqlConnect;
    $columns = [];
    $values = [];
    foreach ($data as $column => $value){
        $values[] = "'".mysqli_real_escape_string($mysqlConnect,$value)."'";
        $columns[] = "`".mysqli_real_escape_string($mysqlConnect,$column)."`";
    }
    $colSets = implode(',',$columns);
    $valSets = implode(',',$values);
    //die("INSERT INTO $table($colSets) VALUES($valSets)");
    $result = mysqli_query($mysqlConnect,"INSERT INTO $table($colSets) VALUES($valSets)");
    return mysqli_insert_id($mysqlConnect);
}

function dbSelect($tableName, $conditionArray = null, $logicalOperator=null, $likeOperator=null, $limit = null, $order=null){
    global $mysqlConnect;
    $query = "SELECT * FROM $tableName";
    if($conditionArray != null) {
        $sets = [];
        foreach ($conditionArray as $column => $value) {
            if($likeOperator != null){
                $sets[] = "`" . mysqli_real_escape_string($mysqlConnect, $column) . "` $likeOperator '%" . mysqli_real_escape_string($mysqlConnect, $value) . "%'";
            }else {
                $sets[] = "`" . mysqli_real_escape_string($mysqlConnect, $column) . "`='" . mysqli_real_escape_string($mysqlConnect, $value) . "'";
            }
        }
        $query .= ' WHERE'.implode($logicalOperator,$sets);
    }
    //die($query);
    $result = mysqli_query($mysqlConnect, $query);
    if (!$result){
        die("SQL error: " . mysqli_error($mysqlConnect)." SQL:".$query);
    }
    return $result->fetch_all(MYSQLI_ASSOC);
}