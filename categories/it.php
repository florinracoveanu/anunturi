<?php
$categoryItems = dbSelect("it_category");
?>

<form method="post" name="myFormName">
	<select class="form-control" name="it_category" onchange="this.form.submit()">
		<option value="" disabled selected>Tip</option>
        <option value="all" >Toate</option><?php
		if(isset($_POST['it_category'])){
			foreach($categoryItems as $categoryItem){?>
				<option value="<?php echo $categoryItem['value']; ?>" <?php if($_POST['it_category'] == $categoryItem['value']) echo 'selected'; ?>><?php echo $categoryItem['name']; ?></option><?php
			}
		}else{
			foreach($categoryItems as $categoryItem){?>
				<option value="<?php echo $categoryItem['value']; ?>"><?php echo $categoryItem['name']; ?></option><?php
			} 
		}?>
	</select>
	<input type="text" class="form-control" placeholder="Pret de la(euro)">
	<input type="text" class="form-control" placeholder="Pret pana la(euro)">
	<br />
</form>